package GastosPiso_Miguel;

import java.util.Date;

/**
 *
 * CLASE DE Gastos del Piso
 * 
 * @author Miguel Páez Hidalgo
 */
public class GastosPiso {

/**
 * variable de tipo String donde se almacena el número de la cuenta bancaria
 * 
 * @param direccionCuenta
 */
    
private String direccionCuenta;

/**
 * variable de tipo double para almacenar el saldo de la cuenta
 * 
 * @param saldo 
 */
private double saldo;

/**
 * Un array de la clase Movimiento
 * 
 * @param Movimiento[]
 */
private final Movimiento[] movimientos;

/**
 * variable de tipo int donde se cuentan los movimientos
 * 
 * @param contadorMovimientos
 */
int contadorMovimientos;

/**
 * 
 * Constructor de la clase GastosPiso
 * 
 * @param numeroPersonas Número de personas en el piso
 * @param direccionCuenta Dirección del piso
 * @param saldo saldo del piso 
 */

public GastosPiso(int numeroPersonas, String direccionCuenta, double saldo) {
    
    this.saldo = saldo;
    this.movimientos = new Movimiento[1000];
    this.contadorMovimientos = 0;
    
}

/**
 * 
 * Método para controlar los posibles errores del programa
 * 
 * @param cantidad
 * @throws IllegalArgumentException 
 * @throws IllegalStateException
 */

public void errores(double cantidad) throws IllegalArgumentException, IllegalStateException {
    if (cantidad <= 0) {throw new IllegalArgumentException("La cantidad a ingresar debe ser mayor que cero.");}
    if (cantidad <= 0) {throw new IllegalArgumentException("La cantidad a retirar debe ser mayor que cero.");}
    if (cantidad > this.saldo) {throw new IllegalStateException("No hay suficiente saldo en la cuenta.");}
}

/**
 * 
 * Método para ingresar dinero en la cuenta
 * 
 * @param cantidad cantidad a ingresar
 * @param concepto nombre del concepto
 */

public void ingresar(double cantidad, String concepto){
    errores(cantidad);
    this.saldo += cantidad;
    this.movimientos[this.contadorMovimientos++] = new Movimiento(cantidad, concepto, new Date(), TipoMovimiento.INGRESO);    
}

/**
 * 
 * Método para ingresar dinero en la cuenta sin concepto ni fecha
 * 
 * @param cantidad 
 */

public void ingresar(double cantidad){
    ingresar(cantidad, "desc");
}
/**
 * 
 * Método para retirar dinero de la cuenta
 * 
 * 
 * @param cantidad cantidad de saldo
 * @param concepto tipo de concepto
 */

public void retirar(double cantidad, String concepto){
    errores(cantidad);
    this.saldo -= cantidad;
    this.movimientos[this.contadorMovimientos++] = new Movimiento(cantidad, concepto, new Date(), TipoMovimiento.GASTO);
}


/**
 * Getter para el número de la cuenta bancaria
 *  
 * @return direccionCuenta número de la cuenta bancaria
 */

public String getDireccionCuenta() {
    return direccionCuenta;
}

/**
 * 
 * Getter para el saldo de la cuenta
 * 
 * @return saldo saldo de la cuenta
 */

public double getSaldo() {
    return this.saldo;
}

/**
 * 
 * Getter del array de Movimientos
 * 
 * @return Movimiento[] movimientos realizados
 */

public Movimiento[] getMovimientos() {
    return this.movimientos;
}

}
