package GastosPiso_Miguel;

import java.util.Date;

/**
 *
 * CLASE DE LOS MOVIMIENTOS
 * 
 * @author Miguel Páez Hidalgo
 */

public class Movimiento {
    
/**
 * Variable de tipo double para almacenar la cantidad a retirar/ingresar
 * 
 * @param cantidad
 */
double cantidad;

/**
 * Variable de tipo String nombre del concepto del movimiento
 * 
 * @param concepto
 */
private String concepto;

/**
 * Variable de tipo Date fecha de la realización del movimiento
 * 
 * @param fecha
 */
private Date fecha;

/**
 * Enum con los tipos de movimientos existentes
 * 
 * @param tipo 
 */
private TipoMovimiento tipo;

/**
 * Constructor de la clase Movimiento
 * 
 * @param cantidad cantidad de saldo
 * @param concepto nombre del concepto
 * @param fecha fecha del movimiento
 * @param tipo tipo de movimiento
 */

public Movimiento(double cantidad, String concepto, Date fecha, TipoMovimiento tipo) {
    
    this.cantidad = cantidad;
    this.concepto = concepto;
    this.fecha = fecha;
    this.tipo = tipo;
}

/**
 * Getter de cantidad
 * 
 * @return cantidad
 */

public double getCantidad() {
    return this.cantidad;
}

/**
 * Getter de concepto
 * 
 * @return concepto
 */

public String getConcepto() {
    return this.concepto;
}

/**
 * Getter de fecha
 * 
 * @return fecha
 */

public Date getFecha() {
    return this.fecha;
}

/**
 * Getter del Tipo de movimiento
 * 
 * @return TipoMovimiento
 */

public TipoMovimiento getTipo() {
    return this.tipo;
}
}
