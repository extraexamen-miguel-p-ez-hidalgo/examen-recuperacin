package GastosPiso_Miguel;

/**
 *
 * CLASE DE TIPO ENUM TiposMovimientos
 * 
 * @author Miguel Páez Hidalgo
 */
public enum TipoMovimiento {
    
    /**
     * Movimiento de tipo Ingreso
     */
    INGRESO,

    /**
     * Movimiento de tipo Gasto
     */
    GASTO
    
}
