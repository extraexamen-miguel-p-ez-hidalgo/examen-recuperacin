/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package GastosPiso_Miguel;

/**
 *
 * @author Miguel Páez Hidalgo
 */
public class Main {

     /**
     * Prueba del programa
     * 
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        // Crea una cuenta
        GastosPiso cuenta1 = new GastosPiso(2, "1234567890", 1000.0);

        // Realización de algunas acciones disponibles en el programa
        try {
            
            cuenta1.ingresar(500.0, "Pago de nómina");
            cuenta1.retirar(150.0, "Compra Marzo");
            cuenta1.ingresar(1500.0, "Ingreso Sueldo");
            cuenta1.retirar(20.0, "Bizum de la Cena");
            
        } catch (Exception e) {
            
            System.out.println(e.getMessage());
        }

        // Imprimir el saldo y los movimientos
        
        System.out.println("Saldo: " + cuenta1.getSaldo() + "\n");
        System.out.println("Movimientos:");
        Movimiento[] movimientos = cuenta1.getMovimientos();
        for (int i = 0; i < cuenta1.contadorMovimientos; i++) {
            Movimiento movimiento = movimientos[i];
            String tipoMovimiento = (movimiento.getTipo() == TipoMovimiento.INGRESO) ? "Ingreso" : "Gasto";
            System.out.println(tipoMovimiento + " de " + movimiento.getCantidad() + " en " + movimiento.getFecha() + " - " + movimiento.getConcepto());
        
        }
    }
    
}
